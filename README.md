IoC
===

Lightweight IoC container.

## Code sample

```
var ioc = require('nan-ioc');

var config = {
  user: {name: 'John Smith'}
};


function B() {
}

B.prototype.sayHello = function () {
  return "When call to aService he says '" + this.a.sayHello() + "'";
};

function C() {
}

C.prototype.sayHello = function () {
  return "When call to aService he says '" + this.a.sayHello() + "'. When call to bService he says '" + this.b.sayHello();
};


ioc.module(['./foo'])
  .component('bService', {
    'class': B,
    'a': ioc.ref('aService')
  })
  .component('cService', {
    'class': C,
    'a': ioc.ref('aService'),
    'b': ioc.ref('bService')
  })
  .build(config);


var c = ioc.get('cService');
console.log(c.sayHello());
```

