/*!
 * Copyright 2015 NaN Labs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
var ioc = require('./../');
var chai = require('chai');
chai.should();
var config = {
  user: {
    name: 'Ezequiel Parada'
  }
};


function B() {}

B.prototype.getName = function() {
  return this.name;
};

B.prototype.getDelegateName = function() {
  return this.a.getName();
};

function C() {}

C.prototype.getName = function() {
  return this.getName;
};

C.prototype.getDelegateName = function() {
  return this.b.getName();
};

C.prototype.getTransitiveDelegateName = function() {
  return this.b.getDelegateName();
};

describe('IOC: \n', function() {

  describe('Config interpolation', function() {
    it('should work as expected', function() {
      c.b.a.getInterpolation().should.equal(config.user.name);
    });
  });

  describe('Associations building', function() {
    var a = "aService";
    var b = "bService";

    it('should get the inmediate delegate name', function() {
      c.getDelegateName().should.equal(b);
    });


    it('should get the transitive delegate test', function() {
      c.getTransitiveDelegateName().should.equal(a);
    });
  });

  describe('Array interpolation', function() {
    var a = "aService"
    it('When referencing an array of components, each component must be created properly', function() {
      c.b.list[0].getName().should.equal(a);
    });
  });
});

ioc.module(['./foo'])
  .component('serviceList', {
    'class': Array,
    '0': ioc.ref('aService')
  })
  .component('bService', {
    'class': B,
    'name': 'bService',
    'a': ioc.ref('aService'),
    'list': ioc.ref('serviceList')
  })
  .component('cService', {
    'class': C,
    'name': 'cService',
    'a': ioc.ref('aService'),
    'b': ioc.ref('bService')
  })
  .build(config);

var c = ioc.get('cService');
