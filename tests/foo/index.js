/*!
 * Copyright 2015 NaNLABS
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
/**
 * Created by ezequiel on 5/1/15.
 */
var ioc = require('../../');


function ServiceFactory() {

}

ServiceFactory.prototype.createObject = function (Class, params) {
  console.log('constructing service');
  return this.createInstance(Class, params);
};

ServiceFactory.prototype.onInitialized = function (ioc) {
  console.log('Container initialized successfully!!!');
};

function A(options) {
}

A.prototype.getName = function () {
  return this.name;
};

A.prototype.getInterpolation = function () {
  return this.interpolation;
};

module.exports = ioc.module()
  .factory('service', {
    'class': ServiceFactory
  })
  .service('aService', {
    'class': A,
    'name': 'aService',
    'interpolation': '${user.name}'
  });
