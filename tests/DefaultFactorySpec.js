'use strict';

var _ = require('lodash');
var chai = require('chai');
var sinon = require('sinon');
var DefaultFactory = require('../lib/DefaultFactory');
var defaultFactory = new DefaultFactory();
chai.should();
var stub = sinon.stub().returns(true);

_.assign(defaultFactory,{
  createInstance: stub
});

describe('Default Factory: \n', function(){
  describe('Create Object: \n',function(){
    it('Should validate the Class type',function(){
      var fn = function(){
        defaultFactory.createObject('class',{_mixins: false})
      };
      fn.should.throw(Error);
      defaultFactory.createObject(DefaultFactory,{_mixins: false}).should.be.true;
    });
  });
});
