/*!
 * Copyright 2015 NaNLABS
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
var path = require('path');
var callsite = require('callsite');
var _ = require('lodash');
var IOC = require('./lib/IOC');


/**
 *
 * @module ioc
 */
module.exports = {

  /**
   * Creates new IOC instance
   * @param dependencies {Array<string>} list of dependant modules
   * @returns {IOC} self instance
   */
  module: function (dependencies) {
    var RELATIVE_MODULE_REGEX = /^\.?\.\/.*$/;
    if (dependencies) {
      var stack = callsite();
      var requester = stack[1].getFileName();
      var dir = path.dirname(requester);
      var children = _.map(dependencies, function (module) {
        if (_.isObject(module) && module.constructor.name === 'IOC') {
          return module;
        } else {
          var modulePath = RELATIVE_MODULE_REGEX.test(module) ? (dir + '/' + module) : module;
          return require(modulePath);
        }
      });
      this._instance = new IOC(children);
    } else {
      this._instance = new IOC();
    }
    return this._instance;
  },

  /**
   * @see {@link IOC#ref}
   * @param name
   * @returns {Definition}
   */
  ref: function (name) {
    return this._instance.ref(name);
  },

  /**
   * @see {@link IOC#get}
   * @param name
   * @returns {*}
   */
  get: function (name) {
    return this._instance.get(name);
  },

  /**
   * @see {@link IOC#find}
   * @param expression
   * @returns {Array.<*>}
   */
  find: function (expression) {
    return this._instance.find(expression);
  }
};
