/*!
 * Copyright 2015 NaNLABS
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
/**
 * Created by ezequiel on 5/1/15.
 */
var _ = require('lodash');
var RefNode = require('./RefNode');

module.exports = Definition;

/**
 * Represents a component definition
 * @param params
 * @constructor
 */
function Definition(name, params, factory) {
  this.name = name;
  this.factory = factory;
  if (params) {
    this.initialize(params);
  }
}

/**
 * Initializes if not initialized otherwise overrides previous values
 * @param definition
 */
Definition.prototype.initialize = function (definition) {
  this.class = definition.class;
  this.properties = _.omitBy(definition, function (value, name) {
    return name === 'class' || value instanceof Definition;
  });
  this.dependencies = _.omitBy(definition, function (value) {
    return !(value instanceof Definition);
  });
  this.initialized = true;
}


/**
 * Builds a RefNode of given definition
 * @returns {RefNode} ref node
 */
Definition.prototype.buildRef = function () {
  return new RefNode(this);
}

/**
 *
 * @returns {boolean}
 */
Definition.prototype.hasDependencies = function () {
  return !_.isEmpty(this.dependencies);
}

