/*!
 * Copyright 2015 NaNLABS
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
/**
 * Created by ezequiel on 5/1/15.
 */
var _ = require('lodash');

module.exports = RefNode;


/**
 *
 * @constant
 * @type {RegExp}
 * @member RefNode
 */
var INTERPOLATION_REGEX = /^\${(.+)}(\?)?$/;

/**
 * Represents a Reference Node. To be used to constructs dependency tree of a component
 * @constructor
 */
function RefNode(definition) {
  this.name = definition.name;
  this._definition = definition;
  this.children = [];
  this.parents = [];
  this.constructed = false;
}

/**
 * Resolves dependency tree
 * @param refs {Object} all available references from Container
 * @param path {Array} path to root of node
 */
RefNode.prototype.resolveTree = function (refs, path) {
  path = path || [this.name];
  if (_.isEmpty(this.children) && this._definition.hasDependencies()) {
    this.children = _.map(this._definition.dependencies, function (dependency) {
      var childRef = refs[dependency.name];
      if (path[0].name === childRef.name) {
        path.push(childRef.name);
        throw new Error('Circular dependency found: ' + path.join(' -> '));
      }
      childRef.addParent(this);
      childRef.resolveTree(refs, path);
      return childRef;
    }.bind(this));
  }
}


/**
 *
 * @param refNode
 */
RefNode.prototype.addParent = function (refNode) {
  this.parents.push(refNode);
}

/**
 * Return true when doesn't have unresolved dependencies
 * @returns {boolean}
 */
RefNode.prototype.ready = function () {
  return _.isEmpty(this.children) || _.every(this.children, 'constructed');
}

/**
 * Creates new instance of referenced definition.
 * Dependencies will be obtained from <code>components</code> and autowired by name
 * @param components
 * @param config
 * @returns {Class}
 */
RefNode.prototype.newInstance = function (components, config) {
  var Class = this._definition.class;

  if (!Class) {
    throw new Error('Unresolved dependency ' + this.name);
  }
  var params = {};

  _.forEach(this._definition.dependencies, function (def, name) {
    params[name] = this.wireDependency(def, name, components);
  }.bind(this));

  _.forEach(this._definition.properties, function (prop, name) {
    params[name] = this.interpolateProperty(prop, config);
  }.bind(this));
  var factory = params['__factory'];
  this.constructed = true; //Do not delete me!. used by reflection in {@link RefNode#ready}
  return factory ? factory.createObject(Class, _.omit(params, '__factory')) : new Class(params);
}

/**
 * @private
 * @param def
 * @param name
 * @param components
 * @returns {*}
 */
RefNode.prototype.wireDependency = function (def, name, components) {
  var dependency = components[def.name];
  if (!dependency) {
    throw new Error('Unresolved dependency ' + def.name +
    ' required by component ' + '"' + this.name);
  }
  return dependency;
}

/**
 * @private
 * @param prop
 * @param name
 * @returns {*}
 */
RefNode.prototype.interpolateProperty = function (prop, config) {
  var value = prop;
  if (_.isString(prop)) {
    var match = INTERPOLATION_REGEX.exec(prop);
    if (match) {
      value = _.get(config, match[1]);
      var optional = !!match[2];
      if (value === undefined && !optional) {
        throw new Error('Missing config ' + prop + ' required by ' + this.name);
      }
    }
  }
  return value;
}
