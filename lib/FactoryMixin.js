/*!
 * Copyright 2015 NaNLABS
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
var _ = require('lodash');
var Mixin = require('nan-mixin');
/**
 *
 * @type {FactoryMixin}
 */
module.exports = Mixin.create('FactoryMixin', {

  /**
   * Creates new instance of Class assigning all parameters passed in constructor
   * as properties
   * @param Class
   * @param params
   * @returns {Class}
   */
  createInstance: function (Class, params) {
    var calledInit = false;
    Class.prototype._init = function () {
      calledInit = true;
      _.assign(this, params);
    };
    if (params._mixins) {
      Mixin.mixOf(Class, params._mixins);
    }
    var instance = new Class(_.omit(params, '_mixins'));
    if (!calledInit) {
      instance._init();
    }
    return instance;
  }
});
