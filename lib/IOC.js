/*!
 * Copyright 2015 NaNLABS
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
/**
 * Created by ezequiel on 4/30/15.
 */
var _ = require('lodash');
var Definition = require('./Definition');
var DefaultFactory = require('./DefaultFactory');
var FactoryMixin = require('./FactoryMixin');

module.exports = IOC;

var DEFAULT_FACTORY = 'default__Factory';
var FACTORY_SUFFIX = '__Factory';

/**
 *
 * @param children
 * @constructor
 */
function IOC(children) {
  this._config = {};
  this._definitions = {};
  this._components = {};
  _.forEach(children, function (child) {
    _.assign(this._definitions, child._definitions);
    this._bindChildFactories(child);
    this.config(child._config);
  }.bind(this));
}

/**
 * Define new component
 * @param name
 * @param params
 * @returns {IOC} self instance
 */
IOC.prototype.component = function (name, params) {
  return this._addDefinition(name, params, DEFAULT_FACTORY);
};

/**
 * Add <b>config</b> object to Container's configuration
 * @param config
 * @returns {IOC}
 */
IOC.prototype.config = function (config) {
  _.merge(this._config, config);
  return this;
};


/**
 * Define new factory
 * @param name
 * @param params
 * @returns {IOC}
 */
IOC.prototype.factory = function (name, params) {
  var factoryName = name + FACTORY_SUFFIX;
  params._mixins = FactoryMixin;
  this._addDefinition(factoryName, params, DEFAULT_FACTORY);
  this[name] = this._bindFactoryMethod(factoryName);
  return this;
};

/**
 * Exposes factory method to be used in ioc chain
 * @param factoryName
 * @private
 */
IOC.prototype._bindFactoryMethod = function (factoryName) {
  return _.partialRight(this._addDefinition, factoryName).bind(this);
};

/**
 * Bind factories defined in child container
 * @param child
 * @private
 */
IOC.prototype._bindChildFactories = function (child) {
  _.forEach(child._definitions, function (def, name) {
    if (_.endsWith(name, FACTORY_SUFFIX)) {
      this[name.replace(FACTORY_SUFFIX, '')] = this._bindFactoryMethod(name);
    }
  }.bind(this));
};

/**
 * References to a component dependency.<br>
 * Usage example:
 * <pre>
 *     ...
 *     .component('userService', {
 *          'class': UserService,
 *          'dataSource': ioc.ref('dataSource')
 *     })
 *     ...
 * </pre>
 * @param name
 * @returns {Definition} reference to specified component
 */
IOC.prototype.ref = function (name) {
  if (!this._definitions[name]) {
    this._definitions[name] = new Definition(name);
  }
  return this._definitions[name];
};

/**
 * Retrieves component instance by name
 * @param name
 * @returns {*} component instance or <code>undefined</code> if <code>name</code> not exists
 */
IOC.prototype.get = function (name) {
  return this._components[name];
};

/**
 * Retrieves an array of matching components
 * @param expression
 * @returns {Array<*>}
 */
IOC.prototype.find = function (expression) {
  var regex = _.isRegExp(expression) ? expression : new RegExp(expression);
  return _.filter(this._components, function (component, name) {
    return regex.test(name);
  });
};

/**
 *
 * @returns {IOC}
 */
IOC.prototype.build = function (config) {
  if (config) {
    this.config(config);
  }
  this._initialize();
  var refs = {};

  _.forEach(this._definitions, function (def, name) {
    refs[name] = def.buildRef();
  });

  _.forEach(refs, function (ref) {
    ref.resolveTree(refs);
  });

  var refList = _.values(refs);

  while (!_.isEmpty((refList))) {
    var nodes = _.remove(refList, function (node) {
      return node.ready();
    });

    _.forEach(nodes, function (node) {
      this._components[node.name] = node.newInstance(this._components, this._config);
    }.bind(this));

  }
  _.forEach(this._components, function (component) {
    if (_.isFunction(component.onInitialized)) {
      component.onInitialized(this);
    }
  }.bind(this));
};

IOC.prototype._addDefinition = function (name, params, factory) {
  var definition = this._definitions[name];
  if (factory) {
    params['__factory'] = this.ref(factory);
  }
  if (!definition) {
    definition = new Definition(name, params);
    this._definitions[name] = definition;
  } else {
    definition.initialize(params);
  }
  return this;
};

/**
 *
 * @private
 */
IOC.prototype._initialize = function () {
  this._addDefinition(DEFAULT_FACTORY, {
    'class': DefaultFactory
  });
};
